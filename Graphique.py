#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#author : Chapeau Marion


# import de toutes les bibliotheques utiles 
import holoviews as hv
import numpy as np
hv.extension('bokeh')
import panel as pn
from bdd import Bdd
import colorcet as cc
import hvplot.pandas 
from datetime import datetime, timedelta
import pandas as pd
from bokeh.models import DatetimeTickFormatter

class Graphique : 

    """Classe qui cree les différents graphiques et le switch qui interagie aqvec ceux-ci
    """
    def __init__(self,chemin_model1,chemin_model2,liste_station):

        """initialise le graphe, le switch et les valeurs asscociées necessaire dans cette classe
        
        Args:
            liste_stations (list): liste des stations
            chemin (string): chemin correspondand au modele represente
        """

        self.chemin = chemin_model1
        self.chemin2 = chemin_model2
        self.liste_station = liste_station 
        self.plots = self.afficher_graphique_1_station()
        self.switch_w= self.switch()
        self.layout_final = self.afficher_composant()
      

    def afficher_graphique_1_station(self):

        """Trace le graphique de la premiere station dans le formulaire pour plusieurs modèles

        Returns:
            holoview plot : graphe avec les différents modèles representes pour une station avec ewwh en ordonne et le temps en abscisse
        """
        plots = hv.Curve([])
        list_model = [self.chemin,self.chemin2]

        for i in range (0,len(list_model)):
            print('boucle')
            num_station = 0
        #date untilisé au hasrad car n'a aucne importnace pour les graphique qui représente les surcharges sur tout lintervalle de temps 
            bdd= Bdd(list_model[i],'2016-02-16 12:00',self.liste_station[num_station])
            grille = bdd.creation_grille_1_station()
            modele = grille['model'][0]
            random_color = np.random.choice(cc.palette['glasbey'], 1)[0]
            points = hv.Points((grille['time'],grille['ewh'])).opts(color=random_color)

            ligne = grille.hvplot.line(x='time', y='ewh', line_width=2, color=random_color, label=f'Model {modele}')


        # Afficher les graphiques
            plots = (plots* points * ligne).opts(
                hv.opts.Curve(show_legend=True, xlabel='Date', ylabel='Valeur EWH en m",title = f'Graphique des modèle de la station {num_station}',width = 1500))

       
        self.plots = pn.pane.HoloViews(plots)
        print('graphe pour une station')
        return self.plots
       

    def afficher_graphique_1_modele(self):
        
        """trace le graphe des statiosn selectionnees pour un modele 

        Returns:
            holoview plot : graphe avec les différentes stations representees pour un modele avec ewwh en ordonne et le temps en abscisse
        """

        plots = hv.Curve([])

        for i in range (0,len(self.liste_station)):
            print('boucle')
            random_color = np.random.choice(cc.palette['glasbey'], 1)[0]
            #date untilisé au hasrad car n'a aucne importnace pour les graphique qui représente les surcharges sur tout lintervalle de temps 
            grille = Bdd(self.chemin,'2016-02-16 12:00',self.liste_station[i]).creation_grille_1_station()
            modele = grille['model'][0]
           
    
            points = hv.Points((grille['time'],grille['ewh'])).opts(color=random_color)
        

            # Utiliser hvplot pour créer une ligne reliant les points
            ligne = grille.hvplot.line(x='time', y='ewh', line_width=2, color=random_color, label=f'station {i+1}')

            # Afficher les graphiques
            plots = ( plots* points * ligne).opts(
        hv.opts.Curve(show_legend=True, xlabel='date', ylabel='Valeur EWH en m',title=f'Graphique des station du modele {modele}',width=1500))

       
        plots = pn.pane.HoloViews(plots)
        print('modele')
        return plots
       
    def switch(self):

        """switch entre les deux representations graphiques

        Returns:
            pn widget :switch station modele 
        """

        switch_w = pn.widgets.Switch(name='Switch')
        #lorsque le switch change valeur True or False on appel la fonction switch change 
        switch_w.param.watch(self.on_switch_change, 'value')
        texte = "Station/modele"
        self.layout = switch_w
        return self.layout
    
    def on_switch_change(self,event):

        """
        efface le graphique précedent et affiche le nouveau en fonction de la valeur du switch

        Args:
            event : changement de valeur du switch
        """

        self.layout.object = None
        if self.switch_w.value==True:
            self.layout_final[1] =hv.Curve([])
            self.layout_final[1] = self.afficher_graphique_1_modele()
            print('switch')
        else :
            self.layout_final[1] =hv.Curve([])
            self.layout_final[1] = self.afficher_graphique_1_station()
            print('switch')

    def update_graphe(self, list_stat):

        """change la valeur de la liste station utlisee dans cette classe cette methode est appelee dans la classe formulaire calcul lorsque qu on 
        lorsqu'on appui sur le bouton valider formulaire ett doncmets a jour les stations representees lors de l'appui sur ce bouton
        Args:
            list_stat (list): liste des nouvelles stations 
        """
       
        self.liste_station = list_stat
    

    

    def afficher_composant (self):
        
        """Methode qui permet de convertir les objets crees en objet panel et d'organiser les objet entre eux sur l'interface

        Returns:
            panel layout : layout qui met le switch au dessus des graphes
        """
    
        self.layout_final =pn.Row( self.switch_w,
        self.plots)


        return self.layout_final
    
    


