#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#author : Chapeau Marion


# import de toutes les bibliotheques utiles 
import numpy as np
import pandas as pd
import holoviews as hv
import panel as pn
pn.extension('tabulator', design='material', template='material', loading_indicator=True)
from holoviews import opts, streams
from holoviews.plotting.links import DataLink
import math


class Formulaire_calcul:
    """classe qui creee l'objet formulaire et les boutons ajouter,supprimer et valider 
    """


    def __init__(self, liste_stations, chemin, Graphique):
        """initialise le formulaire et les valeurs utiles associées 

        Args:
            liste_stations (list): liste des stations
            chemin (string): chemin correspondand au modele represente
            Graphique (class): Classe graphique qui trace les graphiques pour un bouton intéragie avec cette classe
        """
        self.graphe = Graphique
        self.chemin = chemin
        self.liste_stations = liste_stations

        #initialisation de l'objet table
        self.table = hv.Table(self.liste_stations, ['longitude', 'latitude'])
        self.table_pane = pn.pane.HoloViews(self.table)
        self.table.opts(editable=True)
        self.creer_bouton_ajouter = self.bouton_ajouter()
        self.creer_bouton_retirer = self.bouton_retirer()
        self.creer_bouton_valider = self.bouton_valider()

    def bouton_ajouter(self):
        """bouton qui permet d appeler la fonction ajouter station au clique 

        Returns:
            _type_: panel.widgets.button.Button
        """
        self.buttonAjouter = pn.widgets.Button(name='Ajouter station', button_type='primary')
        self.buttonAjouter.on_click(self.ajouter_station)
        

    def bouton_retirer(self):
        """bouton qui permet d appeler la fonction retirer station au clique 
        """
        self.buttonRetirer = pn.widgets.Button(name='Retirer station', button_type='success')
        self.buttonRetirer.on_click(self.retirer_station)
        

    def bouton_valider(self):
        """ bouton qui valide les stations du formulaire
        et appel au clique la fonction valider formulaire 
        """
        self.buttonValider = pn.widgets.Button(name='Valider formulaire', button_type='danger')
        self.buttonValider.on_click(self.valider_formulaire)
        

    def retirer_station(self, on_click):
        """retrirer une station au cilque sur le bouton retirer station

        Args:
            on_click (event): clique sur le bouton retirer station
        """
        #on verifie si le formulaire n'est pas vide 
        if len(self.table.data)!=0:
            self.table.data = self.table.data.iloc[:-1]
            self.update_table()
        else : 
            print('Aucune station a retirer du formulaire')
     

    def valider_formulaire(self, on_click):
        """valider la liste des stations du formulaire pour le tracer des graphes quand on clique sur le bouton valider 

        Args:
            on_click (event): clique sur le bouton valider 
        """
        
        list = self.table.data[['longitude', 'latitude']].values.tolist()
        self.graphe.update_graphe(list)

    def ajouter_station(self, on_click):
        """la fonction ajoute une ligne a la table du formulaire au clique sur ajouter station

        Args:
            on_click (event): clique sur le bouton ajouter station

        """
        # Ajoute une nouvelle ligne au tableau avec comme valeur par defaut [0,0]
        nouvelle_station = {'longitude': 0, 'latitude': 0}
        #cretation du dataframe avec nouvelle station
        nouvelle_ligne_df = pd.DataFrame([nouvelle_station]) 
        #concatenation du dataframe du formulaire et de la nouvelle station 
        self.table.data = pd.concat([self.table.data, nouvelle_ligne_df], ignore_index=True)
        self.update_table()

    def update_table(self):
        """ Mise a jour des valeurs du tableau sur l interface 
        """
        self.table_pane.object = hv.Table(self.table.data, ['longitude', 'latitude']).opts(editable=True)
    
    
    


    def creer_formulaire(self):
        """creation de tout les objets du formualire 

        Returns:
            layout, holoviiew.point : Formulaire et boutons, plot des markers
        """
        

        #initialisation des markesr avec la liste des stations initialisees
        data = {'longitude': [station[0] for station in self.liste_stations],
                'latitude': [station[1] for station in self.liste_stations]}
        #tacer des point
        points = hv.Points(data, kdims=['longitude', 'latitude']).redim.range(x=(-180, 180), y=(-90, 90))
        #point draw qui permet d'ajouter les points au clique 
        streams.PointDraw(data=points.columns(), num_objects=10, source=points)
        #recupération des coordonnées des markers au clique 
        tap_stream = streams.Tap(source=points, x=np.nan, y=np.nan)

        def ajouter_marker(x, y):
            """ajout des coordonnées des marker dans la table du formulaire 

            Args:
                x (int): longitude du marker
                y (int): latitude du marker
            """
            if not math.isnan(x):
                print(int(x),int(y))
                nouvelle_station = {'longitude': int(x), 'latitude': int(y)}
                nouvelle_station = pd.DataFrame([nouvelle_station])
                self.table.data = pd.concat([self.table.data, nouvelle_station], ignore_index=True)
                self.update_table()
            else :
                pass
            
            
        (points).opts(
            opts.Layout(merge_tools=False),
            opts.Points(active_tools=['point_draw'], height=900,
                       size=10, tools=['hover'], width=1500))
        #on lie la table et le graphe des markers
        DataLink(points, self.table)

        #on cree le formulaire qui correspind a la table  
        self.table.opts(editable=True)
        self.table_pane = pn.pane.HoloViews(self.table)

        #organisation de l interface avec la table et les boutons
        self.layout = pn.Column(pn.Row(self.table_pane, align = 'center'),
            pn.Row(self.buttonAjouter, self.buttonRetirer, align ='center'),
            pn.Row(self.buttonValider, align='center'),
            #on lie la fonction ajout marker a la table 
            pn.bind(ajouter_marker, x=tap_stream.param.x, y=tap_stream.param.y),
            width=400
        )

        
        print('formulaire cree')
        return self.layout, points
        
    

    
