import numpy as np 
import pandas as pd
import netCDF4 as nc
from datetime import datetime, timedelta

class Bdd: 

    def __init__(self,cheminFichier,time,liste_station):

        """classe qui extrait les information des donnes netcdf 

        Args:
            cheminFichier (str): chemin du fichier netcdf
            time_ (str): temps pour lequel on veutextraire les valeurs ewh
            station (list): liste de longeur 2 longitude latitude 
        """
        self.time_ = time
        self.station = liste_station
        self.cheminFichier = cheminFichier 
        


    def creation_grille_1_time(self):
        """ouvre le ficher Netcdf et extrait les données pour un temps 

        Returns:
            DataFrame: dataframe de 5 colonnes modele, latitude, longitude, time, ewh de 65341 ligne correspondant a chaque point de la grille.
        """

    # Ouvrir le fichier NetCDF4
    
        dataset = nc.Dataset(self.cheminFichier, 'r')

    # Accéder aux variables et dimensions
        load_variable = dataset.variables['Load']
        time_variable = dataset.variables['time']
        lat_variable = dataset.variables['lat']
        lon_variable = dataset.variables['lon']
        model_var = dataset.variables['model']

    # Lire les données de la variable 'Load'
        load_data = load_variable[:]

    # Lire les valeurs des dimensions
        time = self.time_convertion(time_variable[:])
        lat = lat_variable[:]
        lon = lon_variable[:]-180
        m=len(lat)
        n=len(lon)

        ind_time = np.where(np.array(time) == self.time_)[0][0]
       
        data = np.empty((m,n), dtype=object)
        load_data = load_variable[:]
        t_ewh =load_data[0, ind_time, :, :]
        for i in range (0,m):
            for j in range(0,n):
                data[i][j]=model_var[0],lat[i],lon[j],time[ind_time],t_ewh[i][j]

        data =  data.reshape(-1, 1)

        self.grilleDonnee = pd.DataFrame([t[0] for t in data],columns = ['model', 'latitude', 'longitude', 'time','ewh'])
        print("grille_creee")
        return self.grilleDonnee
    
    def creation_grille_1_station(self):
        """ouvre le ficher Netcdf et extrait les données pour une station longitude,latitude) donc point un point de la grille et tout les temps 

        Returns:
            DataFrame: dataframe de 5 colonnes modele, latitude, longitude, time, ewh de 146 lignes correspondant à un point de la grille et tout les temps.
        """
        
        # Ouvrir le fichier NetCDF4
    
        dataset = nc.Dataset(self.cheminFichier, 'r')

    # Accéder aux variables et dimensions
        load_variable = dataset.variables['Load']
        time_variable = dataset.variables['time']
        lat_variable = dataset.variables['lat']
        lon_variable = dataset.variables['lon']
        model_var = dataset.variables['model']
        

    # Lire les données de la variable 'Load'
        load_data = load_variable[:]

    # Lire les valeurs des dimensions
        time = self.time_convertion(time_variable[:])
        lat = lat_variable[:]
        lon = lon_variable[:]-180

        appel = (lat == self.station[1])
        ind_lat = np.ma.where(appel)[0][0]
        ind_long = np.ma.where(lon == self.station[0])[0][0]
        lat_s = self.station[1]
        lon_s = self.station[0]
        data = np.empty((len(time),5), dtype=object)

        for i in range (0,len(time)) :
            data[i][0] = model_var[0]
            data[i][1] = lat_s
            data[i][2] = lon_s
            data[i][3] = time[i]
            data[i][4] = load_data[0, i, ind_lat, ind_long]

        

        self.grilleDonnee_b = pd.DataFrame(data,columns = ['model', 'latitude', 'longitude', 'time','ewh'])
        print("grille_creee")

        return self.grilleDonnee_b
    
    def time_convertion (self,time_list):

        """connvertir une liste de str en liste de datetime

        Returns:
            list: liste de datetime
        """
        init = datetime(2004,1,16,12,0) 
        conv_date_list = []

        for day in time_list:
            date = init + timedelta(int(day))
            final_date = date.strftime("%Y-%m-%d %H:%M")
            conv_date_list.append(final_date)
        return conv_date_list
    
    def list_time(self):
        """methode qui prend la listes des temps présents dans le netcdf et en sort une liste de date time au bon format 
        Returns:
            list: liste des temps au format datetime
        """
        dataset = nc.Dataset(self.cheminFichier, 'r')
        time_variable = dataset.variables['time']
        time = self.time_convertion(time_variable[:])
        return time

            

    
