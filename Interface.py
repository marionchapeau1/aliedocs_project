import panel as pn 

from Formulaire_calcul import Formulaire_calcul
from Graphique import Graphique
from Carte import Carte

class Interface :
    def __init__(self,chemin,chemin2,liste_station_initiale):
        """_summary_

        Args:
            chemin (string): chemin du fichier netcdf qui contient le modele
            liste_station_initiale (list): liste de staion qu'on veut voir utlisé a l'ouverture de l'application
        """

        self.chemin = chemin
        self.chemin2 = chemin2
        self.station = liste_station_initiale

    def affichage_interface(self):
        """methode qui organise les different objet sur la fenetre d'affichage et gère les interaction entre ces objets
        """
        self.graphe = Graphique(self.chemin,self.chemin2,self.station)
        self.graphe_layout = self.graphe.afficher_composant()
        self.form = Formulaire_calcul(self.station,self.chemin,self.graphe)
        self.marker = self.form.creer_formulaire()[1]
        self.formulaire = self.form.creer_formulaire()[0]
        self.carte = Carte(self.chemin,self.station,self.marker).Afficher_carte()
        self.interface = pn.Column(pn.Row(self.carte,self.formulaire),self.graphe_layout )
        pn.serve(self.interface)

if __name__ == "__main__":
    #rentrer chemin ou se trouve votre fichier data
    chemin1 = input("rentrez le chemin complet vers les données du modele 1 ")
    chemin2 = input("rentrez le chemin complet vers les données du modele 2 ")
    #exemple de station 
    liste_station_initale = [[12,30],[0,0]]
    affichage = Interface(chemin1,chemin2,liste_station_initale).affichage_interface()
    