#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#author : Chapeau Marion


# import de toutes les bibliotheques utiles 
import panel as pn
import geoviews as gv
from geoviews.tile_sources import EsriImagery
import holoviews as hv
hv.extension("bokeh")
import geopandas as gpd
from bdd import Bdd 


class Carte : 

    """classe qui creee la carte avec le time range en interaction avec la carte 
    """

    def __init__(self,chemin,liste_stations,marker):

        """initialise la carte et les valeurs asscociées necessaire dans cette classe

        Args:
            liste_stations (list): liste des stations
            chemin (string): chemin correspondand au modele represente
            marker (holoview point): marker correspondant à la liste des station, un objet retourner pas la methode creer formulaire de la classe formulaire de clalcul
        """
        self.chemin = chemin
        self.marker = marker
        self.liste_stations = liste_stations 
        self.grid_layout = self.Afficher_carte()
        
    def tracer_carte(self,time):
        """ methode qui creee la carte de visualisation des surcharges dans le monde pour une date donnee

        Args:
            time (string): date année-mois-jour heure-minutes des données de surchage representer sur la carte
        Returns:
            hvplot: carte tracer avec les surcharges un fond de carte du monde et les marker des stations initalisees 
        """
        

        df =Bdd(self.chemin,time,[0,0]).creation_grille_1_time()
        scatter = df.hvplot.scatter('longitude','latitude', c='ewh', cmap='RdBu_r', grid=True, responsive = True,active_tools=['point_draw'],alpha=0.5).opts(height=900,width=1700)
        countries = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
        world = countries.sample(5)
        world = countries.hvplot(geo=True, color='white', line_color='black', line_width=0.5,projection= 'PlateCarree',global_extent=True).opts(height=900,width=1700)
        

        plots = (world*scatter*self.marker)
        plots.opts(height=900,width=1500)

        print("tracer carte")
        return plots

   

    def time_filter(self):
        """widget pour modifier la date du trace de la carte de facon iteractive

        Returns:
            pn.widget: time range pour modifier la date du trace de la carte de facon iteractive
        """

        liste_temps = Bdd(self.chemin,0,[0,0]).list_time()
        self.time_range = pn.widgets.DiscreteSlider(name='time', options = liste_temps, value = '2016-02-16 12:00',width=900)
        #faire le lien entre cette methode et la methode pn_time_filter_change lorsque la valeur de temps est modifie par l utilisateur 
        self.time_range.param.watch(self.on_time_filter_change, ["value"])
        return self.time_range
    



    def on_time_filter_change(self,event):

        """Cette methode trace une nouvelle carte avec la valeur de temps changer 
        """
        
        new_values = event.new
        
        self.grid_layout[1] = hv.Curve([])
        self.grid_layout[1] = self.tracer_carte(new_values)


    def Afficher_carte(self):

        """Methode qui permet de convertir les objets crees en objet panel et d'organiser les objet entre eux sur l'interface

        Returns:
            panel layout : layout qui ou la barre de temps est au dessus de la carte
        """

        #la date utilise est une date au hasrad pour unitialiser la carte
        self.plot = self.tracer_carte('2016-02-16 12:00')
        carte = pn.pane.HoloViews(self.plot)
       
        time = self.time_filter()

        self.grid_layout = pn.Column(time, carte)

        return self.grid_layout


